﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.ObjectOrientedProgramming.Interfaces
{
    public interface IFish
    {
        void BreathingUnderwater();
    }
}
