﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetSharePointTeam.ObjectOrientedProgramming.Interfaces;
using DotNetSharePointTeam.ObjectOrientedProgramming.AbstractClasses;

namespace DotNetSharePointTeam.ObjectOrientedProgramming.Entities
{
    public class Dog : Animal, IFish
    {
        public void BreathingUnderwater()
        {
            Console.WriteLine("The dog is breathing underwater");
        }
    }
}
