﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace DotNetSharePointTeam.DelegatesEventsMultiThreading
{
    class ApplicationStart
    {
        [STAThread()]
        static void Main()
        {
            Application app = new Application();

           MainWindow mainWindow = new MainWindow();
           mainWindow.Loaded += StartAction;

           app.Run(mainWindow);
        }

        static void StartAction(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = (MainWindow)e.OriginalSource;

            Kernel.Init();
            Kernel.Instance.OnPushMessage += mainWindow.ProcessingMessage;
            Kernel.Instance.OnStartThread += mainWindow.StartThread;
            mainWindow.OnChangeStatement += Kernel.Instance.ChangeStatement;
            Kernel.Instance.Run();
        }
    }
}