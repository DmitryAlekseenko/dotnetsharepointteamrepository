﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetSharePointTeam.ObjectOrientedProgramming.Interfaces;

namespace DotNetSharePointTeam.ObjectOrientedProgramming.AbstractClasses
{
    public abstract class Fish : LivingEntity, IFish
    {
        public virtual void BreathingUnderwater()
        {
            Console.WriteLine("Some fish is breathing underwater");
        }
    }
}
