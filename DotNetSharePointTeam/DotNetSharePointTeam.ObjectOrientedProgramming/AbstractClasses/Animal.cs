﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.ObjectOrientedProgramming.AbstractClasses
{
    public abstract class Animal : LivingEntity
    {
        public override Int32 CountLegs
        {
            get
            {
                return 4;
            }
        }
    }
}
