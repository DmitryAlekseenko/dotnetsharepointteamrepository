﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.DelegatesEventsMultiThreading
{
    struct Message
    {
        public String ThreadName;
        public String TypeException;
        public String ExceptionMessage;
        public String Test;
    }
}
