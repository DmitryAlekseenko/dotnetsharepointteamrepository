﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DotNetSharePointTeam.Linq.Core;
using DotNetSharePointTeam.Linq.Core.Entities;

namespace DotNetSharePointTeam.Linq.UI.UserControls
{
    /// <summary>
    /// Interaction logic for UCFoto.xaml
    /// </summary>
    public partial class UCFoto : UserControl
    {
        #region Members

        private Window parentWindow;

        #endregion

        #region Constructor

        public UCFoto()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties

        public String Foto { get; set; }

        #endregion

        #region Methods

        private void FotosLoad()
        {
            if (Directory.Exists(Utils.fotoPath))
            {
                String[] files = Directory.GetFiles(Utils.fotoPath, "*.jpg");

                List<FotoEntity> fotos = new List<FotoEntity>();

                foreach (String file in files)
                {
                    fotos.Add(new FotoEntity(file));
                }

                this.lstFoto.ItemsSource = fotos;
            }
        }

        #endregion

        #region Events

        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            FotoEntity foto = (FotoEntity)this.lstFoto.SelectedItem;

            if (foto != null)
            {
                this.Foto = foto.FileName;
                this.parentWindow.Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.parentWindow.Close();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.parentWindow = (Window)this.Parent;
            this.FotosLoad();
        }

        #endregion
    }
}
