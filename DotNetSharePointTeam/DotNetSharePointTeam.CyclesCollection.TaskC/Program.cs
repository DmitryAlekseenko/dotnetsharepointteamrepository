﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.CyclesCollection.TaskC
{
    class Program
    {
        static void AddInt32ItemTest()
        {
            List<Int32> listInt32Item = new List<Int32>();
            ArrayList arrayListInt32Item = new ArrayList();

            Stopwatch timer = Stopwatch.StartNew();

            for (Int32 i = 0; i < 10000000; i++)
            {
                listInt32Item.Add(i);
            }

            timer.Stop();
            Console.WriteLine("Add int32 item to List => {0} ms", timer.ElapsedMilliseconds);

            timer.Restart();

            for (Int32 i = 0; i < 10000000; i++)
            {
                arrayListInt32Item.Add(i);
            }

            timer.Stop();
            Console.WriteLine("Add int32 item to ArrayList => {0} ms", timer.ElapsedMilliseconds);
        }

        static void AddStringItemTest()
        {
            List<String> listStringItem = new List<String>();
            ArrayList arrLstStringItem = new ArrayList();

            Stopwatch timer = Stopwatch.StartNew();

            for (Int32 i = 0; i < 1000000; i++)
            {
                listStringItem.Add(i.ToString());
            }

            timer.Stop();
            Console.WriteLine("Add String item to List => {0} ms", timer.ElapsedMilliseconds);

            timer.Restart();

            for (Int32 i = 0; i < 1000000; i++)
            {
                arrLstStringItem.Add(i.ToString());
            }

            timer.Stop();
            Console.WriteLine("Add String item to ArrayList => {0} ms", timer.ElapsedMilliseconds);
        }

        static void GetInt32ItemTest()
        {
            List<Int32> listInt32Item = new List<Int32>();
            ArrayList arrayListInt32Item = new ArrayList();

            for (Int32 i = 0; i < 1000000; i++)
            {
                listInt32Item.Add(i);
            }

            Stopwatch timer = Stopwatch.StartNew();

            for (Int32 i = 0; i < 1000000; i++)
            {
                Int32 value = listInt32Item[i];
            }

            timer.Stop();
            Console.WriteLine("Get Int32 item from List => {0} ms", timer.ElapsedMilliseconds);


            for (Int32 i = 0; i < 1000000; i++)
            {
                arrayListInt32Item.Add(i);
            }

            timer.Restart();

            for (Int32 i = 0; i < 1000000; i++)
            {
                var value = arrayListInt32Item[i];
            }

            timer.Stop();
            Console.WriteLine("Get Int32 item from ArrayList => {0} ms", timer.ElapsedMilliseconds);
        }

        static void GetStringItemTest()
        {
            List<String> listInt32Item = new List<String>();
            ArrayList arrayListInt32Item = new ArrayList();

            for (Int32 i = 0; i < 1000000; i++)
            {
                listInt32Item.Add(i.ToString());
            }

            Stopwatch timer = Stopwatch.StartNew();

            for (Int32 i = 0; i < 1000000; i++)
            {
                String value = listInt32Item[i];
            }

            timer.Stop();
            Console.WriteLine("Get String item from List => {0} ms", timer.ElapsedMilliseconds);


            for (Int32 i = 0; i < 1000000; i++)
            {
                arrayListInt32Item.Add(i.ToString());
            }

            timer.Restart();

            for (Int32 i = 0; i < 1000000; i++)
            {
                var value = arrayListInt32Item[i];
            }

            timer.Stop();
            Console.WriteLine("Get String item from ArrayList => {0} ms", timer.ElapsedMilliseconds);
        }

        static void SortInt32ItemTest()
        {
            List<Int32> listInt32Item = new List<Int32>();
            ArrayList arrayListInt32Item = new ArrayList();

            for (Int32 i = 1000000; i > 0; i--)
            {
                listInt32Item.Add(i);
            }

            Stopwatch timer = Stopwatch.StartNew();
            listInt32Item.Sort();
            timer.Stop();
            Console.WriteLine("Sort Int32 item from List => {0} ms", timer.ElapsedMilliseconds);


            for (Int32 i = 1000000; i > 0; i--)
            {
                arrayListInt32Item.Add(i);
            }

            timer.Restart();
            arrayListInt32Item.Sort();
            timer.Stop();
            Console.WriteLine("Sort Int32 item from ArrayList => {0} ms", timer.ElapsedMilliseconds);
        }

        static void SortStringItemTest()
        {
            List<String> listStringItem = new List<String>();
            ArrayList arrayListStringItem = new ArrayList();

            for (Int32 i = 1000000; i > 0; i--)
            {
                listStringItem.Add(i.ToString());
            }

            Stopwatch timer = Stopwatch.StartNew();
            listStringItem.Sort();
            timer.Stop();
            Console.WriteLine("Sort String item from List => {0} ms", timer.ElapsedMilliseconds);


            for (Int32 i = 1000000; i > 0; i--)
            {
                arrayListStringItem.Add(i.ToString());
            }

            timer.Restart();
            arrayListStringItem.Sort();
            timer.Stop();
            Console.WriteLine("Sort String item from ArrayList => {0} ms", timer.ElapsedMilliseconds);
        }

        static void Main(string[] args)
        {
            AddInt32ItemTest();
            Console.WriteLine();

            AddStringItemTest();
            Console.WriteLine();

            GetInt32ItemTest();
            Console.WriteLine();

            GetStringItemTest();
            Console.WriteLine();

            SortInt32ItemTest();
            Console.WriteLine();

            SortStringItemTest();
            Console.ReadKey();
        }
    }
}
