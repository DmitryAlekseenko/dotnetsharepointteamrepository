﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.Linq.Core
{
    public static class Utils
    {
        public static readonly String fotoPath;

        static Utils()
        {
            fotoPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "Foto");
        }

        public static Int32 ToInt32(this String str)
        {
            Int32 value;
            return Int32.TryParse(str, out value) ? value : 0;
        }
    }
}
