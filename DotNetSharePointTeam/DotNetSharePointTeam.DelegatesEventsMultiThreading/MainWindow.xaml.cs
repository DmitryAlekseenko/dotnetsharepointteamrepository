﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DotNetSharePointTeam.DelegatesEventsMultiThreading
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Members

        public Action<String> OnChangeStatement;

        #endregion

        #region Constructor

        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        private void SetGUI(String threadName, Boolean isRun)
        {
            if (threadName == Kernel.THREAD_A)
            {
                if (isRun)
                {
                    this.lblThreadA.Content = "Registration of exeptions in thread A is on";
                    this.btnThreadA.Content = "Pause";
                    this.btnThreadA.Background = Brushes.Green;
                }
                else
                {
                    this.lblThreadA.Content = "Registration of exeptions in thread A is off";
                    this.btnThreadA.Content = "Run";
                    this.btnThreadA.Background = Brushes.Red;
                }
            }
            else
            {
                if (isRun)
                {
                    this.lblThreadB.Content = "Registration of exeptions in thread B is on";
                    this.btnThreadB.Content = "Pause";
                    this.btnThreadB.Background = Brushes.Green;
                }
                else
                {
                    this.lblThreadB.Content = "Registration of exeptions in thread B is off";
                    this.btnThreadB.Content = "Run";
                    this.btnThreadB.Background = Brushes.Red;
                }
            }
        }

        public void ProcessingMessage()
        {
            //for testing
            String n0 = Kernel.Instance.Repository.Peek().ThreadName;
            String n1 = Thread.CurrentThread.Name;
            if (n0 != n1)
            {
            }

            this.Dispatcher.Invoke(DispatcherPriority.Normal, new ThreadStart(() =>
            {
                Message message = Kernel.Instance.Repository.Dequeue();
                this.txtMessages.Text += String.Format("{0}\t{1}\t{2}\n", message.ThreadName, message.TypeException, message.ExceptionMessage);
            }));
        }

        public void StartThread(String threadName)
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, new ThreadStart(() =>
            {
                if (threadName == Kernel.THREAD_A) this.btnThreadA.IsEnabled = true;
                else this.btnThreadB.IsEnabled = true;

                this.SetGUI(threadName, false);
            }));
        }

        #endregion

        #region Events

        private void btnThreadA_Click(object sender, RoutedEventArgs e)
        {
            if (this.OnChangeStatement != null) this.OnChangeStatement(Kernel.THREAD_A);
            this.SetGUI(Kernel.THREAD_A, Kernel.Instance.IsThreadA_regExp);
        }

        private void btnThreadB_Click(object sender, RoutedEventArgs e)
        {
            if (this.OnChangeStatement != null) this.OnChangeStatement(Kernel.THREAD_B);
            this.SetGUI(Kernel.THREAD_B, Kernel.Instance.IsThreadB_regExp);
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            this.txtMessages.Text = String.Empty;
        }

       #endregion
    }
}
