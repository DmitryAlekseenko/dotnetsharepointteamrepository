﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetSharePointTeam.ObjectOrientedProgramming.Interfaces;
using DotNetSharePointTeam.ObjectOrientedProgramming.AbstractClasses;

namespace DotNetSharePointTeam.ObjectOrientedProgramming.Entities
{
    public class Crucian : Fish, IHorse
    {
        public override void BreathingUnderwater()
        {
            Console.WriteLine("Crucian is breathing underwater");
        }

        public void EatingHay()
        {
            Console.WriteLine("The Crucian is eating hay");
        }
    }
}
