﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetSharePointTeam.ObjectOrientedProgramming.AbstractClasses;

namespace DotNetSharePointTeam.ObjectOrientedProgramming.Entities
{
    public class Roach : Fish
    {
        public override void BreathingUnderwater()
        {
            Console.WriteLine("Roach is breathing underwater");
        }
    }
}
