﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.Linq.Core.Entities
{
    public class Contact
    {
        public Int32 ContactId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String HomePhone { get; set; }
        public String MobilePhone { get; set; }
        public String Group { get; set; }
        public String Foto { get; set; }
    }
}
