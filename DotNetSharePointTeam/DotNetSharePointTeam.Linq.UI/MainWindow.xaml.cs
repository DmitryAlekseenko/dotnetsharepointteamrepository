﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using DotNetSharePointTeam.Linq.Core;
using DotNetSharePointTeam.Linq.Core.Entities;
using DotNetSharePointTeam.Linq.UI.UserControls;

namespace DotNetSharePointTeam.Linq.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Members

        private List<Contact> contacts;
        private Contact currentContact;
        private FormMode formMode;

        #endregion

        #region Constructor

        public MainWindow()
        {
            InitializeComponent();

            this.formMode = FormMode.Non;
            this.SetForm();
            this.contacts = new List<Contact>();
            this.lstContacts.ItemsSource = this.contacts;
            this.ListTypeViewLoad();
            this.ListContactsLoad();
        }

        #endregion

        private Boolean IsTypeViewGroup { get { return (TypeView)this.cmbTypeView.SelectedValue == TypeView.Group ? true : false; } }

        #region Methods

        private void ListTypeViewLoad()
        {
            List<Object> lstTypeView = new List<Object>();
            lstTypeView.Add(new { Name = "Grid", Value = TypeView.Grid });
            lstTypeView.Add(new { Name = "Group", Value = TypeView.Group });

            this.cmbTypeView.ItemsSource = lstTypeView;
            this.cmbTypeView.DisplayMemberPath = "Name";
            this.cmbTypeView.SelectedValuePath = "Value";
            this.cmbTypeView.SelectedIndex = 0;
        }

        private void ListContactsLoad(String query = "", IEnumerable<Contact> contacts = null)
        {
            this.contacts.Clear();

            if (contacts != null)
                this.contacts.AddRange(contacts);
            else this.contacts.AddRange(DataManager.Instance.GetAllContacts(query));

            this.lstContacts.Items.Refresh();

            this.SetGridView(this.IsTypeViewGroup);
        }

        private void SetForm()
        {
            this.lblFirstNameValid.Visibility = System.Windows.Visibility.Collapsed;
            this.lblLastNameValid.Visibility = System.Windows.Visibility.Collapsed;

            switch (this.formMode)
            {
                
                case FormMode.New:
                case FormMode.Edit:
                    this.lblFirstName.Visibility = System.Windows.Visibility.Collapsed;
                    this.txtFirstName.Visibility = System.Windows.Visibility.Visible;
                    this.txtFirstName.Text = this.currentContact.FirstName;

                    this.lblLastName.Visibility = System.Windows.Visibility.Collapsed;
                    this.txtLastName.Visibility = System.Windows.Visibility.Visible;
                    this.txtLastName.Text = this.currentContact.LastName;

                    this.lblHomePhone.Visibility = System.Windows.Visibility.Collapsed;
                    this.txtHomePhone.Visibility = System.Windows.Visibility.Visible;
                    this.txtHomePhone.Text = this.currentContact.HomePhone;

                    this.lblMobilePhone.Visibility = System.Windows.Visibility.Collapsed;
                    this.txtMobilePhone.Visibility = System.Windows.Visibility.Visible;
                    this.txtMobilePhone.Text = this.currentContact.MobilePhone;

                    this.lblGroup.Visibility = System.Windows.Visibility.Collapsed;
                    this.txtGroup.Visibility = System.Windows.Visibility.Visible;
                    this.txtGroup.Text = this.currentContact.Group;

                    this.lblFoto.Content = this.currentContact.Foto;
                    this.lblFoto.Visibility = System.Windows.Visibility.Visible;
                    this.btnFoto.Visibility = System.Windows.Visibility.Visible;
                    this.imgFoto.Source = String.IsNullOrEmpty(this.currentContact.Foto) ? null : new BitmapImage(new Uri(System.IO.Path.Combine(Utils.fotoPath, this.currentContact.Foto)));
                    break;
                case FormMode.View:
                    this.lblFirstName.Visibility = System.Windows.Visibility.Visible;
                    this.txtFirstName.Visibility = System.Windows.Visibility.Collapsed;
                    this.lblFirstName.Content = this.currentContact.FirstName;

                    this.lblLastName.Visibility = System.Windows.Visibility.Visible;
                    this.txtLastName.Visibility = System.Windows.Visibility.Collapsed;
                    this.lblLastName.Content = this.currentContact.LastName;

                    this.lblHomePhone.Visibility = System.Windows.Visibility.Visible;
                    this.txtHomePhone.Visibility = System.Windows.Visibility.Collapsed;
                    this.lblHomePhone.Content = this.currentContact.HomePhone;

                    this.lblMobilePhone.Visibility = System.Windows.Visibility.Visible;
                    this.txtMobilePhone.Visibility = System.Windows.Visibility.Collapsed;
                    this.lblMobilePhone.Content = this.currentContact.MobilePhone;

                    this.lblGroup.Visibility = System.Windows.Visibility.Visible;
                    this.txtGroup.Visibility = System.Windows.Visibility.Collapsed;
                    this.lblGroup.Content = this.currentContact.Group;

                    this.lblFoto.Content = String.Empty;
                    this.lblFoto.Visibility = System.Windows.Visibility.Collapsed;
                    this.btnFoto.Visibility = System.Windows.Visibility.Collapsed;
                    this.imgFoto.Source = String.IsNullOrEmpty(this.currentContact.Foto) ? null : new BitmapImage(new Uri(System.IO.Path.Combine(Utils.fotoPath, this.currentContact.Foto)));
                    break;
                case FormMode.Non:
                default:
                    this.currentContact = null;

                    this.lblFirstName.Visibility = System.Windows.Visibility.Collapsed;
                    this.lblFirstName.Content = String.Empty;
                    this.txtFirstName.Visibility = System.Windows.Visibility.Collapsed;
                    this.txtFirstName.Text = String.Empty;

                    this.lblLastName.Visibility = System.Windows.Visibility.Collapsed;
                    this.lblLastName.Content = String.Empty;
                    this.txtLastName.Visibility = System.Windows.Visibility.Collapsed;
                    this.txtLastName.Text = String.Empty;

                    this.lblHomePhone.Visibility = System.Windows.Visibility.Collapsed;
                    this.lblHomePhone.Content = String.Empty;
                    this.txtHomePhone.Visibility = System.Windows.Visibility.Collapsed;
                    this.txtHomePhone.Text = String.Empty;

                    this.lblMobilePhone.Visibility = System.Windows.Visibility.Collapsed;
                    this.lblMobilePhone.Content = String.Empty;
                    this.txtMobilePhone.Visibility = System.Windows.Visibility.Collapsed;
                    this.txtMobilePhone.Text = String.Empty;

                    this.lblGroup.Visibility = System.Windows.Visibility.Collapsed;
                    this.lblGroup.Content = String.Empty;
                    this.txtGroup.Visibility = System.Windows.Visibility.Collapsed;
                    this.txtGroup.Text = String.Empty;

                    this.lblFoto.Content = String.Empty;
                    this.lblFoto.Visibility = System.Windows.Visibility.Collapsed;
                    this.btnFoto.Visibility = System.Windows.Visibility.Collapsed;
                    break;
            }
        }

        private void NewContact()
        {
            this.currentContact = new Contact();
            this.formMode = FormMode.New;
            this.SetForm();
        }

        private void EditContact(Contact contact)
        {
            this.formMode = FormMode.Edit;
            this.currentContact = contact;
            this.SetForm();
        }

        private void ViewContact(Int32 contactId)
        {
            this.formMode = FormMode.View;
            this.currentContact = DataManager.Instance.GetContact(contactId);
            this.SetForm();
        }

        private Int32 SaveContact()
        {
            Int32 contactId = 0;

            if (this.currentContact != null)
            {
                this.currentContact.FirstName = this.txtFirstName.Text.Trim();
                this.currentContact.LastName = this.txtLastName.Text.Trim();
                this.currentContact.HomePhone = this.txtHomePhone.Text.Trim();
                this.currentContact.MobilePhone = this.txtMobilePhone.Text.Trim();
                this.currentContact.Group = String.IsNullOrWhiteSpace(this.txtGroup.Text.Trim()) ? "no group" : this.txtGroup.Text.Trim();
                this.currentContact.Foto = this.currentContact.Foto == null ? String.Empty : this.currentContact.Foto;

                if (this.formMode == FormMode.Edit) contactId = DataManager.Instance.Edit(this.currentContact);
                else if (this.formMode == FormMode.New) contactId = DataManager.Instance.New(this.currentContact);
            }

            return contactId;
        }

        private void DeleteContact(Int32 contactId)
        {
            DataManager.Instance.Delete(contactId);
            this.ListContactsLoad();
            this.formMode = FormMode.Non;
            this.SetForm();
        }

        private void SetGridView(Boolean isGroup)
        {
            CollectionView contactsCollection = (CollectionView)CollectionViewSource.GetDefaultView(this.lstContacts.ItemsSource);
            contactsCollection.GroupDescriptions.Clear();

            if (isGroup)
            {
                PropertyGroupDescription groupDescription = new PropertyGroupDescription("Group");
                contactsCollection.GroupDescriptions.Add(groupDescription);
            }
        }

        private void Find()
        {
            String query = this.txtFind.Text.Trim();

            if (!String.IsNullOrWhiteSpace(query))
            {
                IEnumerable <Contact> contacts = DataManager.Instance.GetAllContacts(query);

                if (contacts.Count() == 0) MessageBox.Show("Nothing found", "Search", MessageBoxButton.OK);
                else this.ListContactsLoad(contacts: contacts);
            }
        }

        private void SetFoto()
        {
            Window popupWindow = new Window();
            popupWindow.Height = 500;
            popupWindow.Width = 800;
            popupWindow.MinHeight = 500;
            popupWindow.MinWidth = 800;
            popupWindow.Title = "Foto";
            UCFoto foto = new UCFoto();
            popupWindow.Content = foto;
            popupWindow.ShowDialog();

            if (!String.IsNullOrEmpty(foto.Foto))
            {
                this.currentContact.Foto = foto.Foto;
                this.lblFoto.Content = this.currentContact.Foto;
                this.imgFoto.Source = new BitmapImage(new Uri(System.IO.Path.Combine(Utils.fotoPath, this.currentContact.Foto)));
            }
        }

        private Boolean CheckValid()
        {
            Boolean isValid = true;

            if (String.IsNullOrWhiteSpace(this.txtFirstName.Text))
            {
                isValid = false;
                this.lblFirstNameValid.Visibility = System.Windows.Visibility.Visible;
            }

            if (String.IsNullOrWhiteSpace(this.txtLastName.Text))
            {
                isValid = false;
                this.lblLastNameValid.Visibility = System.Windows.Visibility.Visible;
            }

            return isValid;
        }

        #endregion

        #region Events

        private void btnNew_Click(object sender, RoutedEventArgs e) { this.NewContact(); }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Contact selectedItem = (Contact)this.lstContacts.SelectedItem;

            if (selectedItem != null)
            {
                this.EditContact(selectedItem);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (this.formMode == FormMode.New || this.formMode == FormMode.Edit)
            {
                if (this.CheckValid())
                {
                    Int32 contactId = this.SaveContact();
                    this.ViewContact(contactId);
                    this.ListContactsLoad();
                }
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Int32 contactId = this.currentContact != null ? this.currentContact.ContactId : 0;

            if (contactId != 0)
            {
                String confirm = String.Format("Do you want delete {0} {1} from list ?", this.currentContact.FirstName, this.currentContact.LastName);

                MessageBoxResult result = System.Windows.MessageBox.Show(
                confirm, "Delete contact",
                MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    this.DeleteContact(contactId);
                }
            }
        }

        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            Contact selectedItem = (Contact)this.lstContacts.SelectedItem;

            if (selectedItem != null)
            {
                this.ViewContact(selectedItem.ContactId);
            }
        }

        private void cmbTypeView_SelectionChanged(object sender, SelectionChangedEventArgs e) { this.SetGridView(this.IsTypeViewGroup); }

        private void btnFind_Click(object sender, RoutedEventArgs e) { this.Find(); }

        private void btnShowAll_Click(object sender, RoutedEventArgs e) { this.ListContactsLoad(); }

        private void btnFoto_Click(object sender, RoutedEventArgs e) { this.SetFoto(); }

        #endregion
    }
}
