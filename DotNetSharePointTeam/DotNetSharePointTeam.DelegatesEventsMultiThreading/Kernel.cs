﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.DelegatesEventsMultiThreading
{
    class Kernel
    {
        #region Members

        public const String THREAD_A = "Thread_A";
        public const String THREAD_B = "Thread_B";

        private static Kernel instance;
        private Queue<Message> repository;
        private Object lockObject;
        private Boolean isThreadA_regExp;
        private Boolean isThreadB_regExp;
        public event Action OnPushMessage;
        public event Action<String> OnStartThread;

        #endregion

        #region Constructor

        private Kernel()
        {
            this.repository = new Queue<Message>();
            this.lockObject = new object();
            this.isThreadA_regExp = this.isThreadB_regExp = false;
        }

        #endregion

        #region Properties

        public static Kernel Instance { get { return instance; } }

        public Queue<Message> Repository { get { return repository; } }

        public Boolean IsThreadA_regExp { get { return this.isThreadA_regExp; } }

        public Boolean IsThreadB_regExp { get { return this.isThreadB_regExp; } }

        #endregion

        #region Methods

        private void CreateException()
        {
            Exception exp;
            Random rnd = new Random();

            switch (rnd.Next(0, 6))
            {
                case 0:
                    exp = new FileNotFoundException();
                    break;
                case 1:
                    exp = new OutOfMemoryException();
                    break;
                case 2:
                    exp = new NullReferenceException();
                    break;
                case 3:
                    exp = new ArgumentNullException();
                    break;
                case 4:
                    exp = new ArgumentOutOfRangeException();
                    break;
                default:
                    exp = new Exception();
                    break;
            }

            throw exp;
        }

        public static void Init() { if (instance == null) instance = new Kernel(); }

        public void Run()
        {
            Thread t0 = new Thread(this.Action);
            t0.Name = THREAD_A;
            t0.IsBackground = true;

            Thread t1 = new Thread(this.Action);
            t1.Name = THREAD_B;
            t1.IsBackground = true;

            t0.Start();
            t1.Start();
        }

        public void Action()
        {
            if (this.OnStartThread != null) this.OnStartThread(Thread.CurrentThread.Name);

            for (;;)
            {
                Thread.Sleep(5000);

                try
                {
                    if (Thread.CurrentThread.Name == THREAD_A && this.isThreadA_regExp || Thread.CurrentThread.Name == THREAD_B && this.isThreadB_regExp)
                    {
                        this.CreateException();
                    }
                }
                catch (Exception exp)
                {
                    Message message = new Message()
                    {
                        ThreadName = Thread.CurrentThread.Name,
                        TypeException = exp.GetType().Name,
                        ExceptionMessage = exp.Message
                    };

                    Monitor.Enter(this.lockObject);

                    this.repository.Enqueue(message);
                    if (this.OnPushMessage != null) this.OnPushMessage();

                    Monitor.Exit(this.lockObject);
                }
            }
        }

        public void ChangeStatement(String threadName)
        {
            if (threadName == THREAD_A) this.isThreadA_regExp = !this.isThreadA_regExp;
            else this.isThreadB_regExp = !this.isThreadB_regExp;
        }

        #endregion
    }
}
