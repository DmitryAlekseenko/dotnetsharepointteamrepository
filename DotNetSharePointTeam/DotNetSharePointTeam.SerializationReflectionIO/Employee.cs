﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace DotNetSharePointTeam.SerializationReflectionIO
{
    [DataContract]
    class Employee
    {
        private String EmployeeID { get { return String.Format("{0}{1}", this.LastName, this.FirstName); } }

        [DataMember]
        private String Address { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String FirstName { get; set; }

        [DataMember]
        public Int32 Age { get; set; }

        [DataMember]
        public String Department { get; set; }
    }
}
