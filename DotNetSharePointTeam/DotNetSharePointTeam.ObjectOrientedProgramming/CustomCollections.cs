﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections;
using DotNetSharePointTeam.ObjectOrientedProgramming.AbstractClasses;

namespace DotNetSharePointTeam.ObjectOrientedProgramming
{
    public class CustomCollections<T> : IEnumerable<T> where T : LivingEntity
    {
        #region Members

        private List<T> list;

        #endregion

        #region Constructor

        public CustomCollections()
        {
            this.list = new List<T>();
        }

        #endregion

        #region Methods

        public void Add(T item)
        {
            this.list.Add(item);
        }

        public bool Remove(T item)
        {
            return this.list.Remove(item);
        }

        public Int32 RemoveAll(Predicate<T> match)
        {
            return this.list.RemoveAll(match);
        }

        public void RemoveAt(Int32 index)
        {
            this.list.RemoveAt(index);
        }

        public void RemoveRange(Int32 index, Int32 count)
        {
            this.RemoveRange(index, count);
        }

        public Int32 GetCountLegs()
        {
            return this.list.Sum(n=> n.CountLegs);
        }

        public void GetEntitiesWhoCanBreathingUnderwater()
        {
            Console.WriteLine("\nEntities who can breathing underwater");

            list.FindAll(n => n.GetType().GetMethods().SingleOrDefault(m => m.Name == "BreathingUnderwater") != null)
                .ForEach(n => Console.WriteLine("{0}\tId = {1}", n.GetType().Name, n.EntityId));
        }

        #endregion

        #region Implement IEnumerable<T>

        public IEnumerator<T> GetEnumerator()
        {
            return this.list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return null;
        }

        #endregion
    }
}
