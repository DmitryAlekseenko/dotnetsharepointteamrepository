﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.Linq.Core.Entities
{
    public class FotoEntity
    {
        private String path;

        public FotoEntity(String path)
        {
            this.path = path;
        }

        public String FileName { get { return System.IO.Path.GetFileName(this.path); } }
        public String FilePath { get { return this.path; } }
    }
}
