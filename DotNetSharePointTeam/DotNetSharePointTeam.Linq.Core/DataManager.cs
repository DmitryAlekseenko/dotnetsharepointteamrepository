﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using DotNetSharePointTeam.Linq.Core.Entities;

namespace DotNetSharePointTeam.Linq.Core
{
    public class DataManager
    {
        private static DataManager instance;
        private XDocument xDoc;
        private readonly String path;

        public static void Init()
        {
            instance = instance ?? new DataManager();
        }

        #region Constructor

        public DataManager()
        {
            this.path = Path.Combine(Environment.CurrentDirectory, "Repository.xml");
            this.xDoc = XDocument.Load(path);
        }

        #endregion

        #region Properties

        public static DataManager Instance { get { return instance; } }

        #endregion

        #region Methods

        private Int32 CreateContactId()
        {
            IEnumerable<Int32> lst = this.xDoc.Descendants("Contact").Select(n => n.Element("ContactId").Value.ToInt32());

            Int32 newContactId = 1;
            while (lst.Any(n => n == newContactId)) { newContactId++; }

            return newContactId;
        }

        public IEnumerable<Contact> GetAllContacts(String query = "")
        {
            return this.xDoc.Root.Elements()
                .Where(n => n.Element("FirstName").Value.Trim().ToUpper().Contains(query.ToUpper()) ||
                                     n.Element("LastName").Value.Trim().ToUpper().Contains(query.ToUpper()))
                .Select(n => new Contact()
            {
                ContactId = n.Element("ContactId").Value.Trim().ToInt32(),
                FirstName = n.Element("FirstName").Value.Trim(),
                LastName = n.Element("LastName").Value.Trim(),
                HomePhone = n.Element("HomePhone").Value.Trim(),
                MobilePhone = n.Element("MobilePhone").Value.Trim(),
                Group = n.Element("Group").Value.Trim(),
                Foto = n.Element("Foto").Value.Trim()
            });
        }

        public Contact GetContact(Int32 contactId)
        {
            XElement xContact = xDoc.Root.Elements().Single(n => n.Element("ContactId").Value == contactId.ToString());

            return new Contact()
            {
                ContactId = xContact.Element("ContactId").Value.Trim().ToInt32(),
                FirstName = xContact.Element("FirstName").Value.Trim(),
                LastName = xContact.Element("LastName").Value.Trim(),
                HomePhone = xContact.Element("HomePhone").Value.Trim(),
                MobilePhone = xContact.Element("MobilePhone").Value.Trim(),
                Group = xContact.Element("Group").Value.Trim(),
                Foto = xContact.Element("Foto").Value.Trim()
            };
        }

        public Int32 Edit(Contact contact)
        {
            XElement xContact = xDoc.Root.Elements().Single(n => n.Element("ContactId").Value == contact.ContactId.ToString());

            XElement field = xContact.Element("FirstName");
            field.Value = contact.FirstName.Trim();

            field = xContact.Element("LastName");
            field.Value = contact.LastName.Trim();

            field = xContact.Element("HomePhone");
            field.Value = contact.HomePhone.Trim();

            field = xContact.Element("MobilePhone");
            field.Value = contact.MobilePhone.Trim();

            field = xContact.Element("Group");
            field.Value = contact.Group.Trim();

            field = xContact.Element("Foto");
            field.Value = contact.Foto.Trim();

            xDoc.Save(this.path);

            return contact.ContactId;
        }

        public void Delete(Int32 contactId)
        {
            XElement xElement = xDoc.Root.Elements().Single(n => n.Element("ContactId").Value.ToInt32() == contactId);

            if (xElement != null)
            {
                xElement.Remove();
                xDoc.Save(this.path);
            }
        }

        public Int32 New(Contact contact)
        {
            Int32 contactId = this.CreateContactId();

            XElement newContact = new XElement(
                "Contact",
                new XElement("ContactId", contactId),
                new XElement("FirstName", contact.FirstName.Trim()),
                new XElement("LastName", contact.LastName.Trim()),
                new XElement("HomePhone", contact.HomePhone.Trim()),
                new XElement("MobilePhone", contact.MobilePhone.Trim()),
                new XElement("Group", contact.Group.Trim()),
                new XElement("Foto", contact.Foto.Trim()));

            XElement xElement = this.xDoc.Elements().Single(n => n.Name == "Contacts");
            xElement.Add(newContact);

            xDoc.Save(this.path);

            return contactId;
        }

        #endregion
    }
}
