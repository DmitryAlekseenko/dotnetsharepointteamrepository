﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DotNetSharePointTeam.Linq.Core;

namespace DotNetSharePointTeam.Linq.UI
{
    class ApplicationStart
    {
        [STAThread()]
        static void Main()
        {
            Application app = new Application();
            DataManager.Init();

            ResourceDictionary rd = new ResourceDictionary();
            rd.Source = new Uri("pack://application:,,,/DotNetSharePointTeam.Linq.UI;component/Dictionary1.xaml");
            app.Resources = rd;

            MainWindow mainWindow = new MainWindow();
            app.Run(mainWindow);
        }
    }
}
