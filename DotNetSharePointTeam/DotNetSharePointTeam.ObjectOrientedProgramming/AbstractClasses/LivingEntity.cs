﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.ObjectOrientedProgramming.AbstractClasses
{
    public abstract class LivingEntity
    {
        #region Members

        private Guid entityId;

        #endregion

        #region Constructor

        public LivingEntity()
        {
            this.entityId = Guid.NewGuid();
        }

        #endregion

        #region Properties

        ///TODO: Task E
        public virtual Int32 CountLegs { get { return 0; } }

        public Guid EntityId { get { return this.entityId; } }

        #endregion
    }
}
