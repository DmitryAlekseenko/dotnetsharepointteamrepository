﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetSharePointTeam.ObjectOrientedProgramming.AbstractClasses;
using DotNetSharePointTeam.ObjectOrientedProgramming.Entities;

namespace DotNetSharePointTeam.ObjectOrientedProgramming
{
    class Program
    {
        static void TaskA()
        {
            Console.WriteLine("\nTask A");

            Roach roach = new Roach();
            Console.WriteLine("Roach\tId = {0}", roach.EntityId);
            Dog dog = new Dog();
            Console.WriteLine("Dog\tId = {0}", dog.EntityId);
        }

        static void TaskB()
        {
            Console.WriteLine("\nTask B");

            List<Fish> fishes = new List<Fish>();
            fishes.Add(new Crucian());
            fishes.Add(new Roach());
            fishes.ForEach(n => n.BreathingUnderwater());

            Horse horse = new Horse();
            horse.EatingHay();
        }

        static void TaskC()
        {
            Console.WriteLine("\nTask C");

            Dog dog = new Dog();
            dog.BreathingUnderwater();

            Crucian crucian = new Crucian();
            crucian.EatingHay();
        }

        static void TaskD_E_F()
        {
            Console.WriteLine("\nTask D");

            Console.WriteLine("Create collection");
            CustomCollections<LivingEntity> list = new CustomCollections<LivingEntity>();
            list.Add(new Crucian());
            list.Add(new Dog());
            list.Add(new Horse());
            list.Add(new Roach());
            list.Add(new Crucian());
            list.Add(new Dog());
            list.Add(new Horse());
            list.Add(new Roach());
            list.ToList().ForEach(n => Console.WriteLine("{0}\tId={1}", n.GetType().Name, n.EntityId));

            Console.WriteLine("\nTask E");
            Console.WriteLine("Count legs of all animals => {0}", list.GetCountLegs());

            Console.WriteLine("\nTask F");
            list.GetEntitiesWhoCanBreathingUnderwater();
        }

        static void Main(string[] args)
        {
            TaskA();
            TaskB();
            TaskC();
            TaskD_E_F();

            Console.ReadKey();
        }
    }
}
