﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.CyclesCollection.TaskD
{
    class CustomType<T> : IComparable<CustomType<T>> where T : struct, IComparable<T>
    {
        #region Members

        private T value;

        #endregion

        #region Constructor

        public CustomType(T value)
        {
            this.value = value;
        }

        #endregion

        #region Properties

        public T Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        #endregion

        #region Methods

        public Int32 CompareTo(CustomType<T> other)
        {
            if (other == null) return 1;
            else return this.value.CompareTo(other.Value);
        }

        #endregion
    }
}
