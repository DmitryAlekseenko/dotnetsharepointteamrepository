﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.CyclesCollection.TaskB
{
    class Program
    {
        static void Run()
        {
            CustomCollections<String> obj = new CustomCollections<String>();
            obj.Add("1");
            obj.Add("2");
            obj.Add("3");
            obj.Add("4");
            obj.Add("5");

            Int32 listCount = obj.ListCount;
            Int32 sortedListCount = obj.SortedListCount;

            obj.Add("6");

            listCount = obj.ListCount;
            sortedListCount = obj.SortedListCount;

            obj.Add("7");
            obj.Add("8");
            obj.Add("9");
            obj.Add("10");

            listCount = obj.ListCount;
            sortedListCount = obj.SortedListCount;

            var value = obj[1];

            obj.RemoveAt(0);
            obj.RemoveAt(0);
            obj.RemoveAt(0);
            obj.RemoveAt(0);
            obj.RemoveAt(0);

            listCount = obj.ListCount;
            sortedListCount = obj.SortedListCount;

            obj.Add("1");

            listCount = obj.ListCount;
            sortedListCount = obj.SortedListCount;
        }

        static void Compare()
        {
            List<Int32> list = new List<Int32>();
            SortedList<Int32, Int32> sortedList = new SortedList<Int32, Int32>();

            Stopwatch timer = Stopwatch.StartNew();
            for (Int32 i = 10000; i > 0; i--) { list.Add(i); }
            timer.Stop();
            Console.WriteLine("Add int32 items into List = {0} ms", timer.ElapsedMilliseconds);

            timer.Restart();
            for (Int32 i = 10000; i > 0; i--) { sortedList.Add(i, i); }
            timer.Stop();
            Console.WriteLine("Add int32 items into SortedList = {0} ms", timer.ElapsedMilliseconds);
        }

        static void Main(string[] args)
        {
            Run();
            Compare();

            Console.ReadKey();
        }
    }
}
