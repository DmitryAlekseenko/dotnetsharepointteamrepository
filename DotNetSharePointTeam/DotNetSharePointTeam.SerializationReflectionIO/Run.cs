﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Reflection;
using System.IO;
using System.Xml;

namespace DotNetSharePointTeam.SerializationReflectionIO
{
    class Run
    {
        #region Members

        private static Run instance;
        private List<Employee> employees;
        private List<Employee> employeesDeserialized;
        private readonly String dirAllEmployees;
        private readonly String dirChosenEmployees;

        #endregion

        #region Constructor

        private Run(IEnumerable<Employee> employees = null)
        {
            this.employees = new List<Employee>();
            this.employeesDeserialized = new List<Employee>();
            this.dirAllEmployees = Path.Combine(Environment.CurrentDirectory, "XMLfiles", "AllEmployees.xml");
            this.dirChosenEmployees = Path.Combine(Environment.CurrentDirectory, "XMLfiles", "ChosenEmployees.xml");
            if (employees != null) this.employees.AddRange(employees);
        }

        #endregion

        #region Methods

        public static void Init(IEnumerable<Employee> employees = null)
        {
            instance = instance ?? new Run(employees);
            instance.Action();
        }

        private void CreateList()
        {
            Employee item = new Employee();
            item.FirstName = "Cristiano";
            item.LastName = "Ronaldo";
            item.Age = 30;
            item.Department = "Real Madrid CF";
            item.GetType().GetProperty("Address",BindingFlags.Instance|BindingFlags.NonPublic)
                .SetValue(item, "Madrid Spain");
            this.employees.Add(item);

            item = new Employee();
            item.FirstName = "Mario";
            item.LastName = "Götze";
            item.Age = 23;
            item.Department = "FC Bayern München";
            item.GetType().GetProperty("Address", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(item, "München Germany");
            this.employees.Add(item);

            item = new Employee();
            item.FirstName = "Javier";
            item.LastName = "Pastore";
            item.Age = 26;
            item.Department = "Paris Saint-Germain";
            item.GetType().GetProperty("Address", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(item, "Paris France");
            this.employees.Add(item);

            item = new Employee();
            item.FirstName = "Luke";
            item.LastName = "Shaw";
            item.Age = 20;
            item.Department = "Manchester United FC";
            item.GetType().GetProperty("Address", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(item, "Manchester England");
            this.employees.Add(item);

            item = new Employee();
            item.FirstName = "Lionel";
            item.LastName = "Messi";
            item.Age = 28;
            item.Department = "FC Barcelona";
            item.GetType().GetProperty("Address", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(item, "Barcelona Spain");
            this.employees.Add(item);

            item = new Employee();
            item.FirstName = "Vitaliy";
            item.LastName = "Buyalskiy";
            item.Age = 22;
            item.Department = "FC Dynamo Kyiv";
            item.GetType().GetProperty("Address", BindingFlags.Instance | BindingFlags.NonPublic)
                .SetValue(item, "Kyiv Ukraine");
            this.employees.Add(item);
        }

        private void ShowData(List<Employee> list)
        {
            Console.WindowWidth = 128;
            Console.WriteLine("{0,2}{1,12}{2,12}{3,4}{4,24}{5,20}{6,20}", "№", "Last Name", "First Name", "Age", "Department", "Address", "Employee ID");
            list.ForEach(n => Console.WriteLine("{0,2}{1,12}{2,12}{3,4}{4,24}{5,20}{6,20}"
                    , this.employeesDeserialized.IndexOf(n) + 1
                    , n.LastName
                    , n.FirstName
                    , n.Age
                    , n.Department
                    , n.GetType().GetProperty("Address", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(n)
                    , n.GetType().GetProperty("EmployeeID", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(n)));
        }

        private void SerializeToXMLfile(String dir, List<Employee> list)
        {
            using (FileStream fileStream = new FileStream(dir, FileMode.Create))
            {
                DataContractSerializer serialize
                    = new DataContractSerializer(typeof(List<Employee>), "Employees", "DotNetSharePointTeam.SerializationReflectionIO");
                serialize.WriteObject(fileStream, list);
                fileStream.Close();
            }
        }

        private void DeserializeFromXMLfile(String dir, ref List<Employee> list)
        {
            using (FileStream fileStream = new FileStream(dir, FileMode.Open))
            {
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fileStream, new XmlDictionaryReaderQuotas());
                DataContractSerializer serialize = new DataContractSerializer(typeof(List<Employee>));

                list = (List<Employee>)serialize.ReadObject(reader, false);

                reader.Close();
                fileStream.Close();
            }
        }

        private void Action()
        {
            if (this.employees.Count == 0) this.CreateList();

            this.SerializeToXMLfile(this.dirAllEmployees, this.employees);
            this.DeserializeFromXMLfile(this.dirAllEmployees, ref this.employeesDeserialized);
            Console.WriteLine("All list");
            this.ShowData(this.employeesDeserialized);

            Console.WriteLine("\nNew list");

            List<Employee> chosenEmployees
                = this.employeesDeserialized.Where(n => n.Age >= 25 && n.Age <= 35)
                .OrderBy(n => n.GetType().GetProperty("EmployeeID", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(n)).ToList();
            this.SerializeToXMLfile(this.dirChosenEmployees, chosenEmployees);

            this.DeserializeFromXMLfile(this.dirChosenEmployees, ref this.employeesDeserialized);
            this.ShowData(this.employeesDeserialized);
        }

        #endregion
    }
}
