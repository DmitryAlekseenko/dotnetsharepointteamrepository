﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetSharePointTeam.ObjectOrientedProgramming.Interfaces;
using DotNetSharePointTeam.ObjectOrientedProgramming.AbstractClasses;

namespace DotNetSharePointTeam.ObjectOrientedProgramming.Entities
{
    public class Horse : Animal, IHorse
    {
        public void EatingHay()
        {
            Console.WriteLine("The Horse is eating hay");
        }
    }
}
