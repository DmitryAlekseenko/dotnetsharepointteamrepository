﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.CyclesCollection.TaskD
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Create list");
            List<CustomType<Int32>> list = new List<CustomType<Int32>>();
            list.Add(new CustomType<Int32>(5));
            list.Add(new CustomType<Int32>(3));
            list.Add(new CustomType<Int32>(1));
            list.Add(new CustomType<Int32>(2));
            list.Add(new CustomType<Int32>(4));

            list.ForEach(n => Console.WriteLine(n.Value));
            Console.WriteLine("\nList after sorting");
            list.Sort();
            list.ForEach(n => Console.WriteLine(n.Value));

            Console.ReadKey();
        }
    }
}
