﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.CyclesCollection.TaskB
{
    class CustomCollections<T>
    {
        #region Members

        private List<T> lCollection;
        private SortedList<Int32, T> slCollection;
        private Boolean isUseSortedList;
        private Int32 key;

        #endregion

        #region Constructor

        public CustomCollections()
        {
            this.lCollection = new List<T>();
            this.slCollection = new SortedList<Int32, T>();
            this.isUseSortedList = false;
            this.key = -1;
        }

        #endregion

        #region Properties

        public Object this[Int32 index]
        {
            get
            {
                return this.GetValue(index);
            }
        }

        public Int32 Count
        {
            get { return this.isUseSortedList ? this.slCollection.Count : this.lCollection.Count; }
        }

        public Int32 ListCount
        {
            get { return this.lCollection.Count; }
        }

        public Int32 SortedListCount
        {
            get { return this.slCollection.Count; }
        }

        #endregion

        #region Methods

        private Object GetValue(Int32 index)
        {
            index = Math.Abs(index);

            if (this.isUseSortedList)
            {
                Int32 count = this.slCollection.Count;
                return count > index ? (Object)this.slCollection.Values[index] : null;
            }
            else
            {
                Int32 count = this.lCollection.Count;
                return count > index ? (Object)this.lCollection[index] : null;
            }
        }

        /// <summary>
        /// Remove item from collection
        /// </summary>
        /// <param name="index">Index of item</param>
        public void RemoveAt(Int32 index)
        {
            if (this.isUseSortedList)
            {
                if (this.slCollection.Count > index)
                {
                    this.slCollection.RemoveAt(index);

                    if (this.slCollection.Count == 5)
                    {
                        this.slCollection.Values.ToList().ForEach(n => this.lCollection.Add(n));
                        this.slCollection.Clear();
                        this.isUseSortedList = false;
                    }
                }
            }
            else
            {
                if (this.lCollection.Count > index)
                {
                    this.lCollection.RemoveAt(index);
                }
            }
        }

        /// <summary>
        /// Add item to collection
        /// </summary>
        /// <param name="item">Item</param>
        public void Add(T item)
        {
            if (this.isUseSortedList)
            {
                this.slCollection.Add(++this.key, item);
            }
            else
            {
                if (this.lCollection.Count == 5)
                {
                    this.lCollection.ForEach(n => this.slCollection.Add(++this.key, n));
                    this.slCollection.Add(++this.key, item);
                    this.lCollection.Clear();
                    this.isUseSortedList = true;
                }
                else
                {
                    this.lCollection.Add(item);
                }
            }
        }

        #endregion
    }
}
