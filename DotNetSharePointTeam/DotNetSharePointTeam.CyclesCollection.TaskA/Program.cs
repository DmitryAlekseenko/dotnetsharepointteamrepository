﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSharePointTeam.CyclesCollection.TaskA
{
    class Program
    {
        static void Print(Int32 value, Boolean isEqual)
        {
            Console.ForegroundColor = isEqual ? ConsoleColor.Green : ConsoleColor.White;
            Console.Write("{0,4}", value);
        }

        static void CycleFor()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Cycle For");

            for (Int32 y = 1; y <= 10; y++)
            {
                for (Int32 x = 1; x <= 10; x++)
                {
                    Print(y * x, y == x);
                }

                Console.WriteLine();
            }
        }

        static void CycleDoWhile()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Cycle Do While");

            Int32 y = 1;

            do
            {
                Int32 x = 1;

                do
                {
                    Print(y * x, y == x);
                    x++;
                } while (x <= 10);

                y++;
                Console.WriteLine();
            } while (y <= 10);
        }

        static void CycleWhile()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Cycle While");

            Int32 y = 1;

            while (y <= 10)
            {
                Int32 x = 1;

                while (x <= 10)
                {
                    Print(y * x, y == x);
                    x++;
                }

                y++;
                Console.WriteLine();
            }
        }

        static void CycleForeach()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Cycle Foreach");

            Int32[] values = new Int32[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            foreach (Int32 y in values)
            {
                foreach (Int32 x in values)
                {
                    Print(y * x, y == x);
                }

                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            CycleFor();
            Console.WriteLine();
            CycleDoWhile();
            Console.WriteLine();
            CycleWhile();
            Console.WriteLine();
            CycleForeach();

            Console.ReadKey();
        }
    }
}
